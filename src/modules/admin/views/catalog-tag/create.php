<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model instance\models\CatalogTag */

?>
<div class="catalog-tag-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
