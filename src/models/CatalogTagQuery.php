<?php

namespace kupi_othodov_ru\module_catalog\models;

/**
 * This is the ActiveQuery class for [[CatalogTag]].
 *
 * @see CatalogTag
 */
class CatalogTagQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return CatalogTag[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CatalogTag|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
