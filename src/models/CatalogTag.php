<?php

namespace kupi_othodov_ru\module_catalog\models;

use amd_php_dev\yii2_components\interfaces\SmartInputInterface;
use amd_php_dev\yii2_components\widgets\form\SmartInput;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%catalog_tag}}".
 *
 * @property integer $id
 * @property integer $id_catalog
 * @property string $name
 * @property string $title
 * @property string $url
 * @property string $h1
 * @property string $breadcrumb
 */
class CatalogTag extends \yii\db\ActiveRecord implements SmartInputInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_tag}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_catalog'], 'exist', 'targetClass' => Catalog::className(), 'targetAttribute' => 'id'],
            [['name', 'title', 'url', 'h1', 'breadcrumb'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_catalog' => 'Каталог',
            'name' => 'Название',
            'title' => 'Тайтл',
            'url' => 'URL',
            'h1' => 'H1',
            'breadcrumb' => 'Breadcrumb',
        ];
    }

    /**
     * @inheritdoc
     * @return CatalogTagQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CatalogTagQuery(get_called_class());
    }

    /**
     * @param string $attribute Название атрибута
     * @return int Тип поля ввода
     */
    public function getInputType($attribute)
    {
        switch ($attribute) {
            case 'id_catalog':
                $result = SmartInput::TYPE_SELECT;
                break;
            case 'name':
            case 'title':
            case 'url':
            case 'h1':
            case 'breadcrumb':
            default:
                $result = SmartInput::TYPE_TEXT;
        }

        return $result;
    }

    /**
     * @param string $attribute Название атрибута
     * @return mixed Данные для поля
     */
    public function getInputData($attribute)
    {
        switch ($attribute) {
            case 'id_catalog':
                $result = ArrayHelper::map(Catalog::findAll(['active' => Catalog::ACTIVE_ACTIVE]), 'id', 'name');
                break;
            default:
                $result = null;
        }

        return $result;
    }

    /**
     * @param string $attribute Название атрибута
     * @return array Опции для поля формы
     */
    public function getInputOptions($attribute)
    {
        return null;
    }

    /**
     * Возвращает связанный каталог
     *
     * @return CatalogQuery|ActiveQuery
     */
    public function getCatalogRelation()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'id_catalog']);
    }
}
