<?php

use amd_php_dev\yii2_components\migrations\Migration;

class m291117_101630_new_fields_tag_name_url extends Migration
{
    public static $tableName = '{{%catalog_catalog}}';

    public function safeUp()
    {
        $this->addColumn(self::$tableName, 'tag_name', 'string');
        $this->addColumn(self::$tableName, 'tag_title', 'string');
        $this->addColumn(self::$tableName, 'tag_url', 'string');
        $this->addColumn(self::$tableName, 'tag_breadcrumb', 'string');
        $this->createIndex('tag_url', self::$tableName, 'tag_url');
    }

    public function safeDown()
    {
        $this->dropIndex('tag_url', self::$tableName);
        $this->dropColumn(self::$tableName, 'tag_name');
        $this->dropColumn(self::$tableName, 'tag_title');
        $this->dropColumn(self::$tableName, 'tag_url');
        $this->dropColumn(self::$tableName, 'tag_breadcrumb');
    }
}
