<?php

use amd_php_dev\yii2_components\migrations\Migration;

class m011217_125940_new_field_tag_h1 extends Migration
{
    public static $tableName = '{{%catalog_catalog}}';

    public function safeUp()
    {
        $this->addColumn(self::$tableName, 'tag_h1', 'string');
        $this->addColumn(self::$tableName, 'menu_link', 'string');
    }

    public function safeDown()
    {
        $this->dropColumn(self::$tableName, 'tag_h1');
        $this->dropColumn(self::$tableName, 'menu_link');
    }
}
