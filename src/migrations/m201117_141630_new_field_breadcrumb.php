<?php

use amd_php_dev\yii2_components\migrations\Migration;

class m201117_141630_new_field_breadcrumb extends Migration
{
    public static $tableName = '{{%catalog_catalog}}';
    public static $newFieldName = 'breadcrumb';

    public function safeUp()
    {
        $this->addColumn(self::$tableName, self::$newFieldName, 'string');
    }

    public function safeDown()
    {
        $this->dropColumn(self::$tableName, self::$newFieldName);
    }
}
