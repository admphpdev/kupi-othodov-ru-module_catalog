<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model instance\models\CatalogTag */

?>
<div class="catalog-tag-update">

    <h1>Редактирование тега "<?= $model->name ?>"</h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
