<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel instance\models\CatalogTagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'title',
            'url',
            'h1',
            // 'breadcrumb',

            [
                'class' => yii\grid\ActionColumn::class,
                'urlCreator' => function ($action, $model, $key, $index) {
                    return \yii\helpers\Url::to(['/catalog/admin/catalog-tag/' . $action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>
    