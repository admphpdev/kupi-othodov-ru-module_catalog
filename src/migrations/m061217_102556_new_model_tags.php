<?php

use amd_php_dev\yii2_components\migrations\Migration;

class m061217_102556_new_model_tags extends Migration
{
    public static $tableName = '{{%catalog_tag}}';

    public function safeUp()
    {
        $this->createTable(self::$tableName, [
            'id' => 'pk',
            'id_catalog' => 'int',
            'name' => 'string',
            'title' => 'string',
            'url' => 'string',
            'h1' => 'string',
            'breadcrumb' => 'string',
        ]);
        $this->createIndex('id_catalog', self::$tableName, 'id_catalog');
        $this->createIndex('url', self::$tableName, 'url');
    }

    public function safeDown()
    {
        $this->dropIndex('url', self::$tableName);
        $this->dropIndex('id_catalog', self::$tableName);
        $this->dropTable(self::$tableName);
    }
}
