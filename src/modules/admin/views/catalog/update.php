<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kupi_othodov_ru\module_catalog\models\Catalog */
/** @var \kupi_othodov_ru\module_catalog\models\CatalogTagSearch $tagsSearchModel */
/** @var \yii\data\ActiveDataProvider $tagsDataProvider */

?>
<div class="catalog-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'tagsSearchModel' => $tagsSearchModel,
        'tagsDataProvider' => $tagsDataProvider,
    ]) ?>

</div>
