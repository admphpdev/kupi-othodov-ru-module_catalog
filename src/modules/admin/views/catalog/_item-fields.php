<?php
/* @var $this yii\web\View */
/* @var $model kupi_othodov_ru\module_catalog\models\Catalog */
/* @var $form yii\widgets\ActiveForm */
/** @var \kupi_othodov_ru\module_catalog\models\CatalogTagSearch $tagsSearchModel */
/** @var \yii\data\ActiveDataProvider $tagsDataProvider */
?>

<div class="row">
    <div class="col-md-6">
        <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'active',
            'label'     => true,
            'form'      => $form,
        ]); ?>
    </div>
    <div class="col-md-6">
        <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'priority',
            'label'     => true,
            'form'      => $form,
        ]); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'name',
            'label'     => true,
            'form'      => $form,
        ]); ?>
    </div>
    <div class="col-md-6">
        <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'name_small',
            'label'     => true,
            'form'      => $form,
        ]); ?>
    </div>
</div>

<?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
    'model'     => $model,
    'attribute' => 'menu_link',
    'label'     => true,
    'form'      => $form,
]) ?>

<?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
    'model'     => $model,
    'attribute' => 'breadcrumb',
    'label'     => true,
    'form'      => $form,
]) ?>

<?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
    'model'     => $model,
    'attribute' => 'id_parent',
    'label'     => true,
    'form'      => $form,
]); ?>

<div class="row">
    <div class="col-md-6">
        <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'url',
            'label'     => true,
            'form'      => $form,
        ]); ?>
    </div>
    <div class="col-md-6">
        <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'meta_title',
            'label'     => true,
            'form'      => $form,
        ]); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'meta_description',
            'label'     => true,
            'form'      => $form,
        ]); ?>
    </div>
    <div class="col-md-6">
        <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'text_small',
            'label'     => true,
            'form'      => $form,
        ]); ?>
    </div>
</div>

<div class="row">
    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'image_full',
        'label'     => true,
        'form'      => $form,
    ]); ?>
</div>

<?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
    'model'     => $model,
    'attribute' => 'text_full',
    'label'     => true,
    'form'      => $form,
]); ?>

<?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
    'model'     => $model,
    'attribute' => \kupi_othodov_ru\module_catalog\models\Catalog::ATTR_IMAGES,
    'label'     => true,
    'form'      => $form,
    'options'   => [
        'action' => \yii\helpers\Url::to(['image-gallery'])
    ]
]); ?>

<?php
    if ($model->isNewRecord || !$model->isRoot()) {
        echo
        \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => \kupi_othodov_ru\module_catalog\models\Catalog::ATTR_MANUFACTURES,
            'label'     => true,
            'form'      => $form,
        ]);
    }
?>

<?php if ($tagsSearchModel && $tagsDataProvider): ?>
<div class="row">
    <div class="col-md-12">
        <label class="control-label">Теги</label>
        <p><a href="<?= \yii\helpers\Url::to(['/catalog/admin/catalog-tag/create', 'id_catalog' => $model->id]) ?>" class="btn btn-default btn-sm">Добавить тег</a></p>
        <?= $this->render('/catalog-tag/_items', ['searchModel' => $tagsSearchModel, 'dataProvider' => $tagsDataProvider]) ?>
    </div>
</div>
<?php endif;